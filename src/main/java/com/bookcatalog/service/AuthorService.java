package com.bookcatalog.service;

import com.bookcatalog.model.Author;
import com.google.inject.Inject;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import java.util.List;

public class AuthorService extends AbstractService<Author> {

    @Inject
    public AuthorService(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @SuppressWarnings("unchecked")
    public List<Author> advancedSearch(String name, String birthDate, String deathDate) {
        Criteria criteria = super.criteria()
                .add(Restrictions.conjunction(
                        Restrictions.ilike("name", name, MatchMode.ANYWHERE),
                        Restrictions.ilike("birthDate", birthDate, MatchMode.ANYWHERE),
                        Restrictions.ilike("deathDate", deathDate, MatchMode.ANYWHERE)
                ))
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return criteria.list();
    }

    public Boolean ifAuthorExists(String name, String birthDate) {
        Boolean authorExists;
        Criteria criteria = super.criteria()
                .add(Restrictions.and(
                        Restrictions.eq("name", name),
                        Restrictions.eq("birthDate", birthDate)
                ));
        if (criteria.list().isEmpty()) {
            authorExists = false;
        } else {
            authorExists = true;
        }
        return authorExists;
    }

}
