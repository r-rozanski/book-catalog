package com.bookcatalog.service;

import com.bookcatalog.model.Book;
import com.google.inject.Inject;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

import java.util.List;

public class BookService extends AbstractService<Book> {

    @Inject
    public BookService(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @SuppressWarnings("unchecked")
    public List<Book> findByAll(
            String title, String author, String series, String genre, String language,
            String publisher, String publicationDate, String isbn, String isbn13
    ) {
        Criteria criteria = super.criteria()
                .createAlias("authors", "aut", JoinType.LEFT_OUTER_JOIN)
                .add(Restrictions.conjunction(
                        Restrictions.ilike("title", title, MatchMode.ANYWHERE),
                        Restrictions.ilike("aut.name", author, MatchMode.ANYWHERE),
                        Restrictions.ilike("series", series, MatchMode.ANYWHERE),
                        Restrictions.ilike("genre", genre, MatchMode.ANYWHERE),
                        Restrictions.ilike("language", language, MatchMode.ANYWHERE),
                        Restrictions.ilike("publisher", publisher, MatchMode.ANYWHERE),
                        Restrictions.ilike("publicationDate", publicationDate, MatchMode.ANYWHERE),
                        Restrictions.ilike("isbn", isbn, MatchMode.ANYWHERE),
                        Restrictions.ilike("isbn13", isbn13, MatchMode.ANYWHERE)
                ))
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return criteria.list();
    }

    @SuppressWarnings("unchecked")
    public List<Book> findByAuthorId(Long authorId) {
        Criteria criteria = super.criteria()
                .createAlias("authors", "aut", JoinType.LEFT_OUTER_JOIN)
                .add(Restrictions.eq("aut.id", authorId))
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return criteria.list();
    }

    public Boolean ifBookExists(String title) {
        Boolean bookExists;
        Criteria criteria = super.criteria()
                .add(Restrictions.eq("title", title));
        if (criteria.list().isEmpty()) {
            bookExists = false;
        } else {
            bookExists = true;
        }
        return bookExists;
    }
}
