package com.bookcatalog.service;

import com.bookcatalog.model.AbstractEntity;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;

import java.util.List;

public abstract class AbstractService<T extends AbstractEntity> extends AbstractDAO<T> {

    public AbstractService(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Long save(T t) {
        return persist(t).getId();
    }

    public void update(T t) {
        save(t);
    }

    public void delete(Long id) { currentSession().delete(this.get(id)); }

    public T findById(Long id) {
        return get(id);
    }

    @SuppressWarnings("unchecked")
    public List<T> findAll() {
        Criteria criteria = super.criteria()
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
        return criteria.list();
    }
}
