package com.bookcatalog;

import com.bookcatalog.exceptions.ConstraintViolationExceptionMapper;
import com.bookcatalog.exceptions.CustomExceptionMapper;
import com.bookcatalog.model.Author;
import com.bookcatalog.model.Book;
import com.bookcatalog.resource.AuthorResource;
import com.bookcatalog.resource.BookResource;
import com.bookcatalog.service.AuthorService;
import com.bookcatalog.service.BookService;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import io.dropwizard.Application;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.hibernate.SessionFactory;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration.Dynamic;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.EnumSet;

public class BookCatalog extends Application<BookCatalogConfiguration>
{
    private void configureCors(Environment environment) {
        Dynamic filter = environment.servlets().addFilter("CORS", CrossOriginFilter.class);
        filter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");
        filter.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM,
                "GET,PUT,POST,DELETE,OPTIONS");
        filter.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");
        filter.setInitParameter(CrossOriginFilter.ACCESS_CONTROL_ALLOW_ORIGIN_HEADER, "*");
        filter.setInitParameter("allowedHeaders",
                "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin");
        filter.setInitParameter("allowCredentials", "true");
    }

    private final HibernateBundle<BookCatalogConfiguration> hibernate =
            new HibernateBundle<BookCatalogConfiguration>(Book.class, Author.class) {
                @Override
                public DataSourceFactory getDataSourceFactory
                        (BookCatalogConfiguration bookCatalogConfiguration) {
                    return bookCatalogConfiguration.getDataSourceFactory();
                }
            };

    private Injector injector;

    public HibernateBundle<BookCatalogConfiguration> getHibernateBundle() {
        return hibernate;
    }

    public Injector getInjector() {
        return injector;
    }

    @Override
    public void initialize(Bootstrap<BookCatalogConfiguration> bootstrap) {

        bootstrap.addBundle(hibernate);
    }

    @Override
    public void run(BookCatalogConfiguration configuration, Environment environment)
            throws Exception {
        injector = (Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                bind(SessionFactory.class).toInstance(getHibernateBundle().getSessionFactory());
                bind(Validator.class)
                        .toInstance(Validation.buildDefaultValidatorFactory().getValidator());
            }
        }));

        environment.jersey().register(injector.getInstance(BookResource.class));
        environment.jersey().register(injector.getInstance(BookService.class));
        environment.jersey().register(injector.getInstance(AuthorResource.class));
        environment.jersey().register(injector.getInstance(AuthorService.class));
        environment.jersey().register(injector.getInstance(Validator.class));

        environment.jersey().register(CustomExceptionMapper.class);
        environment.jersey().register(ConstraintViolationExceptionMapper.class);


        SessionFactory sessionFactory = injector.getBinding(SessionFactory.class)
                .getProvider().get();
        InitialData initialData = new InitialData(sessionFactory);
        initialData.generateData();

        configureCors(environment);
    }

    public static void main( String[] args ) throws Exception {
        new BookCatalog().run(args);
    }
}
