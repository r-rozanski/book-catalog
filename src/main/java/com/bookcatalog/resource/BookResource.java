package com.bookcatalog.resource;

import com.bookcatalog.dto.BookPersistedDto;
import com.bookcatalog.dto.BookToPersistDto;
import com.bookcatalog.exceptions.CustomException;
import com.bookcatalog.groups.FullValidation;
import com.bookcatalog.model.Book;
import com.bookcatalog.service.BookService;
import com.google.inject.Inject;
import io.dropwizard.hibernate.UnitOfWork;
import io.dropwizard.validation.Validated;

import javax.validation.constraints.NotNull;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;


@Path("/book")
@Produces(MediaType.APPLICATION_JSON)
public class BookResource {

    private BookService bookService;

    @Inject
    public BookResource(BookService bookService) {
        this.bookService = bookService;
    }

    @GET
    @UnitOfWork
    @Path("/{id}")
    public Response getBook(@PathParam("id") @NotNull Long id) throws Exception {
        Book book = bookService.findById(id);
        if (book == null) {
            throw new CustomException("Book not found.");
        }
        BookPersistedDto persisted = new BookPersistedDto(
                book.getId(), book.getTitle(), book.getAuthors(), book.getSeries(),
                book.getSeriesPart(), book.getGenre(), book.getLanguage(), book.getPages(),
                book.getPublisher(), book.getPublicationDate(), book.getIsbn(),
                book.getIsbn13(), book.getDescription());
        return Response.ok().entity(persisted).build();
    }

    @GET
    @UnitOfWork
    public Response getAllBooks() {
        List<Book> books = bookService.findAll();
        return Response.ok().entity(books).build();
    }

    @GET
    @UnitOfWork
    @Path("/author/{authorId}")
    public Response getBooksByAuthor(@PathParam("authorId") @NotNull Long authorId) {
        List<Book> books = bookService.findByAuthorId(authorId);
        return Response.ok().entity(books).build();
    }

    @GET
    @UnitOfWork
    @Path("/search")
    public Response advancedSearch(
            @QueryParam("title") String title,
            @QueryParam("author") String author,
            @QueryParam("series") String series,
            @QueryParam("genre") String genre,
            @QueryParam("lang") String language,
            @QueryParam("pub") String publisher,
            @QueryParam("pub_date") String publicationDate,
            @QueryParam("isbn") String isbn,
            @QueryParam("isbn13") String isbn13
    ) {
        List<Book> books = bookService.findByAll(title, author, series, genre, language,
                publisher, publicationDate, isbn, isbn13);
        return Response.ok().entity(books).build();
    }

    @POST
    @UnitOfWork
    public Response createBook(@Validated(FullValidation.class) BookToPersistDto dto) throws Exception{
        Book book = new Book(dto.getTitle(), dto.getAuthors(), dto.getSeries(),
                dto.getSeriesPart(), dto.getGenre(), dto.getLanguage(), dto.getPages(),
                dto.getPublisher(), dto.getPublicationDate(), dto.getIsbn(),
                dto.getIsbn13(), dto.getDescription());
        if (bookService.ifBookExists(book.getTitle())) {
            throw new CustomException("Book already exists.");
        }
        Long id = bookService.save(book);
        return Response.status(Response.Status.CREATED).entity(id).build();
    }

    @PUT
    @Path("/{id}")
    @UnitOfWork
    public Response update(@PathParam("id") @NotNull Long id,
                           @Validated(FullValidation.class) BookToPersistDto dto) {
        Book book = bookService.findById(id);
        book.setTitle(dto.getTitle());
        book.setAuthors(dto.getAuthors());
        book.setSeries(dto.getSeries());
        book.setSeriesPart(dto.getSeriesPart());
        book.setGenre(dto.getGenre());
        book.setLanguage(dto.getLanguage());
        book.setPages(dto.getPages());
        book.setPublisher(dto.getPublisher());
        book.setPublicationDate(dto.getPublicationDate());
        book.setIsbn(dto.getIsbn());
        book.setIsbn13(dto.getIsbn13());
        book.setDescription(dto.getDescription());
        bookService.update(book);
        return Response.noContent().entity(id).build();
    }

    @DELETE
    @Path("/{id}")
    @UnitOfWork
    public Response deleteBook(@PathParam("id") @NotNull Long id) throws Exception {
        Book book = bookService.findById(id);
        if (book == null) {
            throw new CustomException("Book not found.");
        }
        bookService.delete(id);
        return Response.noContent().entity(id).build();
    }
}
