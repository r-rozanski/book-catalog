package com.bookcatalog.resource;

import com.bookcatalog.dto.AuthorPersistedDto;
import com.bookcatalog.dto.AuthorToPersistDto;
import com.bookcatalog.exceptions.CustomException;
import com.bookcatalog.groups.FullValidation;
import com.bookcatalog.model.Author;
import com.bookcatalog.model.Book;
import com.bookcatalog.service.AuthorService;
import com.bookcatalog.service.BookService;
import com.google.inject.Inject;
import io.dropwizard.hibernate.UnitOfWork;
import io.dropwizard.validation.Validated;

import javax.validation.constraints.NotNull;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/author")
@Produces(MediaType.APPLICATION_JSON)
public class AuthorResource {

    private AuthorService authorService;
    private BookService bookService;

    @Inject
    public AuthorResource(AuthorService authorService, BookService bookService) {
        this.authorService = authorService;
        this.bookService = bookService;
    }

    @GET
    @UnitOfWork
    @Path("/{id}")
    public Response getAuthor(@PathParam("id") @NotNull Long id) throws Exception{
        Author author = authorService.findById(id);
        if (author == null) {
            throw new CustomException("Author not found.");
        }
        AuthorPersistedDto persisted = new AuthorPersistedDto(author.getId(), author.getName(),
                author.getBirthDate(), author.getDeathDate());
        return Response.ok().entity(persisted).build();
    }

    @GET
    @UnitOfWork
    public Response getAll() {
        List<Author> authors = authorService.findAll();
        return Response.ok().entity(authors).build();
    }

    @GET
    @UnitOfWork
    @Path("/search")
    public Response getAuthorAdvSearch(
            @QueryParam("name") String name,
            @QueryParam("birth_date") String birthDate,
            @QueryParam("death_date") String deathDate) {
        List<Author> authors = authorService.advancedSearch(name, birthDate, deathDate);
        return Response.ok().entity(authors).build();
    }

    @POST
    @UnitOfWork
    public Response createAuthor(@Validated(FullValidation.class) AuthorToPersistDto dto) throws Exception {
        Author author = new Author(dto.getName(), dto.getBirthDate(), dto.getDeathDate());
        if (authorService.ifAuthorExists(author.getName(), author.getBirthDate())) {
            throw new CustomException("Author already exists.");
        }
        Long id = authorService.save(author);
        return Response.status(Response.Status.CREATED).entity(id).build();
    }

    @PUT
    @Path("/{id}")
    @UnitOfWork
    public Response update(@PathParam("id") @NotNull Long id,
                           @Validated(FullValidation.class) AuthorToPersistDto dto) {
        Author author = authorService.findById(id);
        author.setName(dto.getName());
        author.setBirthDate(dto.getBirthDate());
        author.setDeathDate(dto.getDeathDate());
        authorService.update(author);
        return Response.noContent().entity(id).build();
    }

    @DELETE
    @Path("/{id}")
    @UnitOfWork
    public Response deleteBook(@PathParam("id") @NotNull Long id) throws Exception {
        List<Book> books = bookService.findByAuthorId(id);
        if (!(books.isEmpty())) {
            String message = "";
            if (books.size() == 1) {
                message = "There is still 1 book connected to this author.";
            } else  {
                message = "There are still " + books.size() + " books connected to this author.";
            }
            throw new CustomException(message);
        }
        Author author = authorService.findById(id);
        if (author == null) {
            throw new CustomException("Author not found.");
        }
        authorService.delete(id);
        return Response.noContent().entity(id).build();
    }
}
