package com.bookcatalog.exceptions;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class CustomExceptionMapper implements ExceptionMapper<CustomException> {

    public CustomExceptionMapper() {

    }

    @Override
    public Response toResponse(CustomException customException) {
        return Response.status(Response.Status.NOT_FOUND).type(MediaType.APPLICATION_JSON).entity(customException.getMessage()).build();
    }
}
