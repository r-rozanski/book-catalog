package com.bookcatalog;

import com.bookcatalog.model.Author;
import com.bookcatalog.model.Book;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class InitialData {

    private SessionFactory sessionFactory;

    @Inject
    public InitialData(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void generateData() {
        Session session = sessionFactory.openSession();

        ObjectMapper mapper = new ObjectMapper();

        // Authors
        try {
            List<Author> authors = mapper.readValue(getClass()
                    .getResource("/initialdata/authors.json"), mapper.getTypeFactory()
                    .constructCollectionType(List.class, Author.class));
            for (Author author : authors) {
                session.save(author);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Books
        try {
            List<Book> books = mapper.readValue(getClass()
                    .getResource("/initialdata/books.json"), mapper.getTypeFactory()
                    .constructCollectionType(List.class, Book.class));
            for (Book book : books) {
                Set<Author> authorsToSave = new HashSet<>();
                for (Author author : book.getAuthors()) {
                    Author newAuthor = (Author) session.get(Author.class, author.getId());
                    authorsToSave.add(newAuthor);
                };
                book.setAuthors(authorsToSave);
                session.save(book);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        session.flush();
        session.close();
    }
}
