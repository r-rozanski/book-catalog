package com.bookcatalog.dto;

import javax.validation.constraints.NotNull;

public class AuthorPersistedDto extends AuthorToPersistDto {

    @NotNull
    private Long id;

    public AuthorPersistedDto() {

    }

    public AuthorPersistedDto(Long id, String name, String birthDate, String deathDate) {
        super(name, birthDate, deathDate);
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
