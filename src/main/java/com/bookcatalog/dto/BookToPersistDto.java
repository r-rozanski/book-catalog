package com.bookcatalog.dto;

import com.bookcatalog.model.Author;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Set;

public class BookToPersistDto implements AbstractDto {

    @NotEmpty
    @Length(max = 255)
    private String title;

    @NotNull
    private Set<Author> authors;

    @NotNull
    @Length(max = 255)
    private String series;

    @NotNull
    private Integer seriesPart;

    @NotEmpty
    @Length(max = 50)
    private String genre;

    @NotEmpty
    @Length(max = 50)
    private String language;

    @NotNull
    @Min(1)
    private Integer pages;

    @NotNull
    @Length(max = 100)
    private String publisher;

    @NotNull
    private String publicationDate;

    @NotNull
    @Length(min = 10, max = 10)
    private String isbn;

    @NotNull
    @Length(min = 13, max = 13)
    private String isbn13;

    @NotNull
    @Length(max = 3000)
    private String description;

    public BookToPersistDto() {
    }

    public BookToPersistDto(String title, Set<Author> authors, String series, Integer seriesPart,
                            String genre, String language, Integer pages, String publisher,
                            String publicationDate, String isbn, String isbn13,
                            String description) {
        this.title = title;
        this.authors = authors;
        this.series = series;
        this.seriesPart = seriesPart;
        this.genre = genre;
        this.language = language;
        this.pages = pages;
        this.publisher = publisher;
        this.publicationDate = publicationDate;
        this.isbn = isbn;
        this.isbn13 = isbn13;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Set<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(Set<Author> authors) {
        this.authors = authors;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public Integer getSeriesPart() {
        return seriesPart;
    }

    public void setSeriesPart(Integer seriesPart) {
        this.seriesPart = seriesPart;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Integer getPages() {
        return pages;
    }

    public void setPages(Integer pages) {
        this.pages = pages;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(String publication_date) {

        this.publicationDate = publication_date;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getIsbn13() {
        return isbn13;
    }

    public void setIsbn13(String isbn13) {
        this.isbn13 = isbn13;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
