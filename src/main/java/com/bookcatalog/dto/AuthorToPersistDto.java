package com.bookcatalog.dto;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

public class AuthorToPersistDto implements AbstractDto {

    @NotEmpty
    @Length(max = 500)
    private String name;

    @NotEmpty
    @Length(max = 50)
    private String birthDate;

    @NotNull
    @Length(max = 50)
    private String deathDate;

    public AuthorToPersistDto() {

    }

    public AuthorToPersistDto(String name, String birthDate, String deathDate) {
        this.name = name;
        this.birthDate = birthDate;
        this.deathDate = deathDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getDeathDate() {
        return deathDate;
    }

    public void setDeathDate(String deathDate) {
        this.deathDate = deathDate;
    }
}
