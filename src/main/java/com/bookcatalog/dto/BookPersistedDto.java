package com.bookcatalog.dto;

import com.bookcatalog.model.Author;

import javax.validation.constraints.NotNull;
import java.util.Set;

public class BookPersistedDto extends BookToPersistDto {

    @NotNull
    private Long id;

    public BookPersistedDto() {
    }

    public BookPersistedDto(Long id, String title, Set<Author> authors, String series,
                            Integer seriesPart, String genre, String language, Integer pages,
                            String publisher, String publicationDate, String isbn, String isbn13,
                            String description) {
        super(title, authors, series, seriesPart, genre, language, pages, publisher,
                publicationDate, isbn, isbn13, description);
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
