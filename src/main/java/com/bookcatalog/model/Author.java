package com.bookcatalog.model;


import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@AttributeOverride(name = "id", column = @Column(name = "author_id"))
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = {"name"})
}, indexes = {
        @Index(columnList = "name", name = "name")
})
public class Author extends AbstractEntity {

    @Column(name = "name", length = 500, nullable = false)
    private String name;

    @Column(name = "birth_date", length = 50, nullable = false)
    private String birthDate;

    @Column(name = "death_date", length = 50, nullable = false)
    private String deathDate;

    public Author() {

    }

    public Author(String name, String birthDate, String deathDate) {
        this.name = name;
        this.birthDate = birthDate;
        this.deathDate = deathDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getDeathDate() {
        return deathDate;
    }

    public void setDeathDate(String deathDate) {
        this.deathDate = deathDate;
    }
}
