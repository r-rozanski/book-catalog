package com.bookcatalog.model;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.util.Set;


@Entity
@AttributeOverride(name = "id", column = @Column(name = "book_id"))
@Table(uniqueConstraints = {
        @UniqueConstraint(columnNames = {"title"})
}, indexes = {
        @Index(columnList = "title", name = "title")
})
public class Book extends AbstractEntity {

    @Column(length = 255, nullable = false)
    private String title;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "author_book",
            joinColumns = @JoinColumn(name = "book_id"),
            inverseJoinColumns = @JoinColumn(name = "author_id")
    )
    private Set<Author> authors;

    @Column(length = 255, nullable = false)
    private String series;

    @Column(name = "series_part", nullable = false)
    private Integer seriesPart;

    @Column(length = 50, nullable = false)
    private String genre;

    @Column(length = 50, nullable = false)
    private String language;

    @Column(nullable = false)
    private Integer pages;

    @Column(length = 100, nullable = false)
    private String publisher;

    @Column(name = "publication_date", nullable = false)
    private String publicationDate;

    @Column(length = 10, nullable = false)
    private String isbn;

    @Column(length = 13, nullable = false)
    private String isbn13;

    @Column(nullable = false, length = 3000)
    private String description;

    public Book() {
    }

    public Book(String title, Set<Author> authors, String series, Integer seriesPart,
                String genre, String language, Integer pages, String publisher,
                String publication_date, String isbn, String isbn13, String description) {
        this.title = title;
        this.authors = authors;
        this.series = series;
        this.seriesPart = seriesPart;
        this.genre = genre;
        this.language = language;
        this.pages = pages;
        this.publisher = publisher;
        this.publicationDate = publication_date;
        this.isbn = isbn;
        this.isbn13 = isbn13;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Set<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(Set<Author> authors) {
        this.authors = authors;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public Integer getSeriesPart() {
        return seriesPart;
    }

    public void setSeriesPart(Integer seriesPart) {
        this.seriesPart = seriesPart;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Integer getPages() {
        return pages;
    }

    public void setPages(Integer pages) {
        this.pages = pages;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(String publicationDate) {
        this.publicationDate = publicationDate;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getIsbn13() {
        return isbn13;
    }

    public void setIsbn13(String isbn13) {
        this.isbn13 = isbn13;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        Book book = (Book) o;
        if (!title.equals(book.title)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + title.hashCode();
        return result;
    }
}
