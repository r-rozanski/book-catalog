var app = angular.module('BookCatalog', ['ngRoute', 'ngSanitize', 'ui.bootstrap', 'ui.select']);

app.config(function(uiSelectConfig) {
    uiSelectConfig.resetSearchInput = true;
});

// filter based on ui.select example
app.filter('propsFilter', function() {
    return function(items) {
        var out = [];
        if (angular.isArray(items)) {
            items.forEach(function(item) {
                out.push(item);
            });
        } else {
            out = items;
        }
        return out;
    };
});

app.controller('MainCtrl', ['$scope', function ($scope) {

}]);

app.controller('BooksCtrl', ['$scope', '$http', '$location', '$modal', function ($scope, $http, $location, $modal) {
    $scope.successMessage = '';
    $scope.errorMessage = '';

    $http.get('http://localhost:8080/book').success(
        function (data) {
            $scope.books = data;
        });

    $scope.showBook = function (book) {
        $location.path('book/' + book.id);
    };

    $scope.editBook = function (book) {
        $location.path('edit_book/' + book.id);
    };

    $scope.askIfDelete = function (book) {
        $scope.modalDetails = {};
        $scope.modalDetails.bookTitle = book.title;
        $scope.modalDetails.bookId = book.id;
        var modalInstance = $modal.open({
            templateUrl: 'partials/ask_delete_book.html',
            controller: 'ModalDeleteCtrl',
            resolve: {
                modalDetails: function () {
                    return $scope.modalDetails;
                }
            }
        });

        modalInstance.result.then(function (errorMessage) {
            $scope.errorMessage = errorMessage;
            if ($scope.errorMessage == '') {
                $scope.successMessage = 'Book '
                + $scope.modalDetails.bookTitle + ' deleted.';
                $("#" + $scope.modalDetails.bookId).remove();
            } else {
                $scope.successMessage = '';
            }
        });
    }
}]);

app.controller('BookCtrl', ['$scope', '$http', '$routeParams', function ($scope, $http, $routeParams) {
    $scope.bookId = $routeParams.bookId;
    $scope.singleAuthor = true;
    $scope.errorMessage = '';

    $http.get('http://localhost:8080/book/' + $scope.bookId)
        .success(function (data) {
            $scope.book = data;
            if ($scope.book.authors.length > 1) {
                $scope.singleAuthor = false;
            }
        })
        .error(function (data) {
            $scope.errorMessage = data.message;
        });
}]);

app.controller('AddBookCtrl', ['$scope', '$http', function ($scope, $http) {
    $scope.bookToAdd = {};
    $scope.errorMessage = '';
    $scope.successMessage = '';
    $scope.showForm = true;
    $scope.allAuthors = [];
    $scope.authors = {};
    $scope.authors.selected = null;

    $http.get('http://localhost:8080/author').success(
        function (data) {
            $scope.allAuthors = data;
        });

    $scope.addBook = function () {
        if ($scope.authors.selected.length == 0) {
            $scope.successMessage = '';
            $scope.errorMessage = 'You must choose at least one author.';
            $('html,body').animate({
                scrollTop: $('.alert-danger').offset().top - 9999
            }, 0);
            return false;
        }
        if (!moment($scope.bookToAdd.publicationDate, 'DD-MM-YYYY').isValid()){
            $scope.successMessage = '';
            $scope.errorMessage = 'Invalid date.';
            $('html,body').animate({
                scrollTop: $('.alert-danger').offset().top - 9999
            }, 0);
        } else {
            $scope.successMessage = '';
            $scope.errorMessage = '';
            $scope.bookToAdd.authors = $scope.authors.selected;
            if ($scope.bookToAdd.series == null) $scope.bookToAdd.series = '';
            if ($scope.bookToAdd.seriesPart == null) $scope.bookToAdd.seriesPart = 0;
            $scope.bookToAdd.description = $scope.bookToAdd.description.replace(/\n/g, '<br>');
            $http.post('http://localhost:8080/book/', $scope.bookToAdd)
                .success(function () {
                    $scope.successMessage = 'Book ' + $scope.bookToAdd.title + ' added.';
                    $scope.errorMessage = '';
                    $scope.showForm = false;
                })
                .error(function (data) {
                    $scope.successMessage = '';
                    if (data.cause == 'ConstraintViolation') {
                        $scope.errorMessage = 'Book with the same title already exists.';
                    } else {
                        $scope.errorMessage = data.message;
                    }
                    $scope.showForm = false;
                });
        }
    }
}]);

app.controller('EditBookCtrl', ['$scope', '$http', '$routeParams', function ($scope, $http, $routeParams) {
    $scope.bookId = $routeParams.bookId;
    $scope.errorMessage = '';
    $scope.successMessage = '';
    $scope.showForm = true;
    $scope.bookToEdit = {};
    $scope.allAuthors = [];
    $scope.authors = {};

    $http.get('http://localhost:8080/author').success(
        function (data) {
            $scope.allAuthors = data;

            $http.get('http://localhost:8080/book/' + $scope.bookId)
                .success(function (data) {
                    $scope.bookToEdit.title = data.title;
                    $scope.authors.selected = data.authors;
                    $scope.bookToEdit.series = data.series;
                    if (data.seriesPart == 0) {
                        $scope.bookToEdit.seriesPart = null;
                    } else {
                        $scope.bookToEdit.seriesPart = data.seriesPart;
                    }
                    $scope.bookToEdit.pages = data.pages;
                    $scope.bookToEdit.genre = data.genre;
                    $scope.bookToEdit.language = data.language;
                    $scope.bookToEdit.publisher = data.publisher;
                    $scope.bookToEdit.publicationDate = moment(data.publicationDate).format('DD-MM-YYYY');
                    $scope.bookToEdit.isbn = data.isbn;
                    $scope.bookToEdit.isbn13 = data.isbn13;
                    $scope.bookToEdit.description = data.description.replace(/<br>/g, '\n').replace(/<br \/>/g, '\n');
                })
                .error(function (data) {
                    $scope.isError = true;
                    $scope.showForm = false;
                    $scope.errorMessage = data.message;
                });
        });

    $scope.editBook = function () {
        if ($scope.bookToEdit.seriesPart == null) $scope.bookToEdit.seriesPart = 0;
        $scope.bookToEdit.authors = $scope.authors.selected;
        if ($scope.authors.selected.length == 0) {
            $scope.successMessage = '';
            $scope.errorMessage = 'You must choose at least one author.';
            $('html,body').animate({
                scrollTop: $('.alert-danger').offset().top - 9999
            }, 0);
            return false;
        }
        if (!moment($scope.bookToEdit.publicationDate, 'DD-MM-YYYY').isValid()) {
            $scope.errorMessage = 'Invalid date.';
            $('html,body').animate({
                scrollTop: $('.alert-danger').offset().top - 9999
            }, 0);
        } else {
            $scope.bookToEdit.publicationDate = moment($scope.bookToEdit.publicationDate, 'DD-MM-YYYY').format();
            $scope.bookToEdit.description = $scope.bookToEdit.description.replace(/\n/g, '<br>');
            $http.put('http://localhost:8080/book/' + $scope.bookId, $scope.bookToEdit)
                .success(function () {
                    $scope.successMessage = 'Book ' + $scope.bookToEdit.title + ' saved.';
                    $scope.errorMessage = '';
                    $scope.showForm = false;
                })
                .error(function (data) {
                    $scope.successMessage = '';
                    if (data.cause == 'ConstraintViolation') {
                        $scope.errorMessage = 'Book with the same title already exists.';
                    } else {
                        $scope.errorMessage = data.message;
                    }
                    $scope.showForm = false;
                });
        }
    };
}]);

app.controller('AuthorsCtrl', ['$scope', '$http', '$location', '$modal', function ($scope, $http, $location, $modal) {
    $scope.successMessage = '';
    $scope.errorMessage = '';

    $http.get('http://localhost:8080/author').success(
        function (data) {
            $scope.authors = data;
        });

    $scope.showAuthor = function (author) {
        $location.path('author/' + author.id);
    };

    $scope.editAuthor = function (author) {
        $location.path('edit_author/' + author.id);
    };

    $scope.askIfDelete = function (author) {
        $scope.modalDetails = {};
        $scope.modalDetails.authorName = author.name;
        $scope.modalDetails.authorId = author.id;
        var modalInstance = $modal.open({
            templateUrl: 'partials/ask_delete_author.html',
            controller: 'ModalDeleteCtrl',
            resolve: {
                modalDetails: function () {
                    return $scope.modalDetails;
                }
            }
        });

        modalInstance.result.then(function (errorMessage) {
            $scope.errorMessage = errorMessage;
            if ($scope.errorMessage == '') {
                $scope.successMessage = 'Author '
                + $scope.modalDetails.authorName + ' deleted.';
                $("#" + $scope.modalDetails.authorId).remove();
            } else {
                $scope.successMessage = '';
            }
        });
    };
}]);

app.controller('AuthorCtrl', ['$scope', '$http', '$routeParams', function ($scope, $http, $routeParams) {
    $scope.authorId = $routeParams.authorId;
    $scope.errorMessage = '';
    $http.get('http://localhost:8080/author/' + $scope.authorId)
        .success(function (data) {
            $scope.author = data;
            $http.get('http://localhost:8080/book/author/' + $scope.authorId).success(
                function (data) {
                    $scope.books = data;
                });
        })
        .error(function (data) {
            $scope.isError = true;
            $scope.errorMessage = data.message;
        });
}]);

app.controller('AddAuthorCtrl', ['$scope', '$http', function ($scope, $http) {
    $scope.authorToAdd = {};
    $scope.errorMessage = '';
    $scope.successMessage = '';
    $scope.showForm = true;

    $scope.addAuthor = function () {
        if ($scope.authorToAdd.deathDate == null) $scope.authorToAdd.deathDate = '';
        if ((!moment($scope.authorToAdd.birthDate, 'DD-MM-YYYY').isValid()) ||
            (($scope.authorToAdd.deathDate != '')
            && (!moment($scope.authorToAdd.deathDate, 'DD-MM-YYYY').isValid()))) {
            $scope.errorMessage = 'Invalid date.';
            $('html,body').animate({
                scrollTop: $('.alert-danger').offset().top - 9999
            }, 0);
        } else {
            $scope.authorToAdd.birthDate = moment($scope.authorToAdd.birthDate, 'DD-MM-YYYY')
                .format();
            if ($scope.authorToAdd.deathDate != '') {
                $scope.authorToAdd.deathDate = moment($scope.authorToAdd.deathDate, 'DD-MM-YYYY')
                    .format();
            }
            $http.post('http://localhost:8080/author/', $scope.authorToAdd)
                .success(function () {
                    $scope.successMessage = 'Author ' + $scope.authorToAdd.name + ' added.';
                    $scope.errorMessage = '';
                    $scope.showForm = false;
                })
                .error(function (data) {
                    $scope.successMessage = '';
                    if (data.cause == 'ConstraintViolation') {
                        $scope.errorMessage = 'Author with the same name already exists.';
                    } else {
                        $scope.errorMessage = data.message;
                    }
                    $scope.showForm = false;
                });
        }
    };
}]);

app.controller('EditAuthorCtrl', ['$scope', '$http', '$routeParams', function ($scope, $http, $routeParams) {
    $scope.authorId = $routeParams.authorId;
    $scope.errorMessage = '';
    $scope.successMessage = '';
    $scope.showForm = true;
    $scope.authorToEdit = {};

    $http.get('http://localhost:8080/author/' + $scope.authorId)
        .success(function (data) {
            $scope.authorToEdit.name = data.name;
            $scope.authorToEdit.birthDate = moment(data.birthDate).format('DD-MM-YYYY');
            if ((data.deathDate != '') && (data.deathDate != '')) {
                $scope.authorToEdit.deathDate = moment(data.deathDate).format('DD-MM-YYYY');
            } else {
                $scope.authorToEdit.deathDate = '';
            }
        })
        .error(function (data) {
            $scope.isError = true;
            $scope.showForm = false;
            $scope.errorMessage = data.message;
        });

    $scope.editAuthor = function () {
        if ($scope.authorToEdit.deathDate == null) $scope.authorToEdit.deathDate = '';
        if ((!moment($scope.authorToEdit.birthDate, 'DD-MM-YYYY').isValid()) ||
            (($scope.authorToEdit.deathDate != '')
            && (!moment($scope.authorToEdit.deathDate, 'DD-MM-YYYY').isValid()))) {
            $scope.errorMessage = 'Invalid date.';
            $('html,body').animate({
                scrollTop: $('.alert-danger').offset().top - 9999
            }, 0);
        } else {
            $scope.authorToEdit.birthDate = moment($scope.authorToEdit.birthDate, 'DD-MM-YYYY')
                .format();
            if ($scope.authorToEdit.deathDate != '') {
                $scope.authorToEdit.deathDate = moment($scope.authorToEdit.deathDate, 'DD-MM-YYYY')
                    .format();
            }
            $http.put('http://localhost:8080/author/' + $scope.authorId, $scope.authorToEdit)
                .success(function () {
                    $scope.successMessage = 'Author ' + $scope.authorToEdit.name + ' saved.';
                    $scope.errorMessage = '';
                    $scope.showForm = false;
                })
                .error(function (data) {
                    $scope.successMessage = '';
                    if (data.cause == 'ConstraintViolation') {
                        $scope.errorMessage = 'Author with the same name already exists.';
                    } else {
                        $scope.errorMessage = data.message;
                    }
                    $scope.showForm = false;
                });
        }
    };
}]);

app.controller('SearchCtrl', ['$scope', '$http', '$location', function ($scope, $http, $location) {
    $scope.minYear = '1450';
    $scope.maxYear = '2015';
    $scope.searchParams = {};
    $scope.searchVisible = true;
    $scope.errorMessage = '';

    $scope.showBook = function (book) {
        $location.path('book/' + book.id);
    };

    $scope.searchBooks = function () {

        if ((($scope.searchParams.title == '') || ($scope.searchParams.title == null))
            && (($scope.searchParams.author == '') || ($scope.searchParams.author == null))
            && (($scope.searchParams.series == '') || ($scope.searchParams.series == null))
            && (($scope.searchParams.genre == '') || ($scope.searchParams.genre == null))
            && (($scope.searchParams.language == '') || ($scope.searchParams.language == null))
            && (($scope.searchParams.publisher == '') || ($scope.searchParams.publisher == null))
            && (($scope.searchParams.year == '') || ($scope.searchParams.year == null))
            && (($scope.searchParams.ISBN == '') || ($scope.searchParams.ISBN == null))
            && (($scope.searchParams.ISBN13 == '') || ($scope.searchParams.ISBN13 == null))) {
            $scope.errorMessage = 'At least one field must not be empty.';
        } else {
            $scope.errorMessage = '';

            if ($scope.searchParams.title == null) $scope.searchParams.title = '';
            if ($scope.searchParams.author == null) $scope.searchParams.author = '';
            if ($scope.searchParams.series == null) $scope.searchParams.series = '';
            if ($scope.searchParams.genre == null) $scope.searchParams.genre = '';
            if ($scope.searchParams.language == null) $scope.searchParams.language = '';
            if ($scope.searchParams.publisher == null) $scope.searchParams.publisher = '';
            if ($scope.searchParams.year == null) $scope.searchParams.year = '';
            if ($scope.searchParams.ISBN == null) $scope.searchParams.ISBN = '';
            if ($scope.searchParams.ISBN13 == null) $scope.searchParams.ISBN13 = '';

            $http.get('http://localhost:8080/book/search?title=' + $scope.searchParams.title
            + '&author=' + $scope.searchParams.author
            + '&series=' + $scope.searchParams.series
            + '&genre=' + $scope.searchParams.genre
            + '&lang=' + $scope.searchParams.language
            + '&pub=' + $scope.searchParams.publisher
            + '&pub_date=' + $scope.searchParams.year
            + '&isbn=' + $scope.searchParams.ISBN
            + '&isbn13=' + $scope.searchParams.ISBN13)
                .success(function (data) {
                    if (data[0] != null) {
                        $scope.books = data;
                        $scope.searchVisible = false;
                    } else {
                        $scope.errorMessage = 'No books found.';
                        $scope.searchVisible = true;
                    }
                })
                .error(function (data) {
                    $scope.errorMessage = data;
                });
        }
    }
}]);

app.controller('ModalDeleteCtrl', ['$scope', '$http', '$modalInstance', 'modalDetails', function ($scope, $http, $modalInstance, $modalDetails) {

    $scope.modalDetails = $modalDetails;
    $scope.errorMessage = '';

    $scope.deleteAuthor = function () {
        $http.delete('http://localhost:8080/author/' + $scope.modalDetails.authorId)
            .success(function () {
                $scope.errorMessage = '';
                $modalInstance.close($scope.errorMessage);
            })
            .error(function (data) {
                $scope.errorMessage = data.message;
                $modalInstance.close($scope.errorMessage);
            });
    };

    $scope.deleteBook = function () {
        $http.delete('http://localhost:8080/book/' + $scope.modalDetails.bookId)
            .success(function () {
                $scope.errorMessage = '';
                $modalInstance.close($scope.errorMessage);
            })
            .error(function (data) {
                $scope.errorMessage = data.message;
                $modalInstance.close($scope.errorMessage);
            });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
}]);


app.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'partials/index.html',
                controller: 'MainCtrl'
            })
            .when('/books', {
                templateUrl: 'partials/books.html',
                controller: 'BooksCtrl'
            })
            .when('/book/:bookId', {
                templateUrl: 'partials/book.html',
                controller: 'BookCtrl'
            })
            .when('/add_book/', {
                templateUrl: 'partials/add_book.html',
                controller: 'AddBookCtrl'
            })
            .when('/edit_book/:bookId', {
                templateUrl: 'partials/edit_book.html',
                controller: 'EditBookCtrl'
            })
            .when('/authors', {
                templateUrl: 'partials/authors.html',
                controller: 'AuthorsCtrl'
            })
            .when('/author/:authorId', {
                templateUrl: 'partials/author.html',
                controller: 'AuthorCtrl'
            })
            .when('/add_author/', {
                templateUrl: 'partials/add_author.html',
                controller: 'AddAuthorCtrl'
            })
            .when('/edit_author/:authorId', {
                templateUrl: 'partials/edit_author.html',
                controller: 'EditAuthorCtrl'
            })
            .when('/search/', {
                templateUrl: 'partials/search.html',
                controller: 'SearchCtrl'
            })
            .otherwise({
                redirectTo: '/'
            });
    }]);
